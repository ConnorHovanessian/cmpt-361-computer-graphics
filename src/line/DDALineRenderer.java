package line;

import geometry.Vertex3D;
import windowing.drawable.Drawable;
import windowing.graphics.Color;


public class DDALineRenderer implements LineRenderer {
	private DDALineRenderer() {}
	

	@Override
	public void drawLine(Vertex3D p1, Vertex3D p2, Drawable drawable) {
		double dx = p2.getX() - p1.getX();
		double dy = p2.getY() - p1.getY();
		
		Color c1 = p1.getColor();
		Color c2 = p2.getColor();
		
		double dr = (c2.getR()-c1.getR())/dx;
		double dg = (c2.getG()-c1.getG())/dx;
		double db = (c2.getB()-c1.getB())/dx;
		
		Color dc = new Color (dr, dg, db);
		Color color = c1;
		
		double m = dy/dx;
		double y = p1.getY();
		
		double dz = p2.getZ() - p1.getZ();
		double zSlope = dz/dx;
		double drawZ = p1.getZ();
		
		for(double x = p1.getX(); x <= p2.getX(); x++) 
		{
			drawable.setPixel((int) (x+.5), (int) (y+.5), drawZ, color.asARGB()); 
			y = y + m;
			color = color.add(dc);
			drawZ+= zSlope;
		}
		
	}
	public static LineRenderer make() {
		return new AnyOctantLineRenderer(new DDALineRenderer());
	}
}