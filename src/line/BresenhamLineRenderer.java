package line;


import geometry.Vertex3D;
import windowing.drawable.Drawable;


public class BresenhamLineRenderer implements LineRenderer {

	private BresenhamLineRenderer() {}

	@Override
	public void drawLine(Vertex3D p1, Vertex3D p2, Drawable drawable) {
		int dx = p2.getIntX() - p1.getIntX();
		int dy = p2.getIntY() - p1.getIntY();
		int argbColor = p1.getColor().asARGB();
		
		int m = 2*dy;
		int q = m-(2*dx);
		int y = p1.getIntY();
		
		drawable.setPixel(p1.getIntX(), y, 0.0, argbColor);
		int err = m - dx;
		
		
		for(int x = p1.getIntX()+1; x <= p2.getIntX(); x++) 
		{
			if (err >= 0)
			{
				err += q;
				y++;
			}
			
			else {err +=m;}
				
			drawable.setPixel(x, y, 0.0, argbColor);
		}
		
	}		

	public static LineRenderer make() {
		return new AnyOctantLineRenderer(new BresenhamLineRenderer());
	}
}