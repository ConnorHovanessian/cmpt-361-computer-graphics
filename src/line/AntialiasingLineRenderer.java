package line;

import geometry.Vertex3D;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class AntialiasingLineRenderer implements LineRenderer{
	private AntialiasingLineRenderer() {}

	
	@Override
	public void drawLine(Vertex3D p1, Vertex3D p2, Drawable drawable) {
		double x1 = p1.getX();
		double x2 = p2.getX();
		double y1 = p1.getY();
		double y2 = p2.getY();
		
		double dx = x2 - x1;
		double dy = y2 - y1;
		Color color = p1.getColor();
		
		double m = dy/dx;
		double y = y1;
		double r = Math.sqrt(2);
//		double r = .5; sqrt 2 looks much better than r=.5
		double r2 = Math.pow(r, 2); 
		
		for(double x = p1.getX(); x <= p2.getX(); x++) 
		{
			for (int i = -1;  i<=1; i++) // 3 pixel column centered at y
			{
				int y0 = (int) (y+i); 
				
				//calculate distance from our pixel (x0,y0) to the line
				double d = Math.abs(( dy*x - dx*y0 + x2*y1 - y2*x1 )) / Math.sqrt(Math.pow((dy),2)+Math.pow((dx),2));
				
//				double d = Math.abs(dx*(y1-y0)-(x1-x)*(dy))/Math.sqrt(dx*dx + dy*dy);
				
				//Our line is unit width, so this reduces d by .5 (since d is perpendicular to the line)
				d -= .5;
				
				double a = Math.acos(d/r);
				
				double area = 1 - (((1-(a/Math.PI))*(Math.PI*r2) + d*Math.sqrt(r2-Math.pow(d, 2))) / (Math.PI*r2)) ;
					
				
				//set pixel using calculated values
				//I tried a few different techniques for calculating color values, but setPixelWithCoverage seemed to look the best
				
//				color = color.scale(area);
//				Color oldColor = Color.fromARGB(drawable.getPixel(x, (int) y0));
//				color = color.blendInto(area, oldColor);
//				drawable.setPixel(x, (int) y0, 0.0, color.asARGB());
				
//				new_color = background_color * (1-opacity) + opacity * target_color
				
				drawable.setPixelWithCoverage((int) (x), (y0), 0.0, color.asARGB(), area);
		
				//NOTE: The diagonal artifact in the center of the starburst test is present even with expensive drawer

			}
			
			y = y + m;
		}
		
	}		

	public static LineRenderer make() {
		return new AnyOctantLineRenderer(new AntialiasingLineRenderer());
	}
}
