package client.testPages;

import java.util.concurrent.ThreadLocalRandom;
import geometry.Vertex3D;
import polygon.Polygon;
import polygon.PolygonRenderer;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class MeshPolygonTest {
	
	private final PolygonRenderer renderer;
	private final Drawable panel;
	private final int margin = 30;
	private final int panelSize = 650;
	private final int interval = 64;
	
	public MeshPolygonTest(Drawable panel, PolygonRenderer renderer, Boolean pert) {
		this.panel = panel;
		this.renderer = renderer;
	
		
		render(pert);
	}
	
	private void render(Boolean pert) {		
		Vertex3D[][] vM = new Vertex3D[10][10];
		int i = 0;
		int j = 0;
		
		if(pert==false)
		{
			for(double x = margin+5; x < panelSize-margin+5; x+=interval) {
				for(double y = margin+5; y < panelSize-margin+5; y+=interval)
				{
					Color pColor = Color.random();
					vM[i][j] = new Vertex3D(x, y-1, 0, pColor);
					j++;
				}
				
			i++;
			j=0;
			}
		}
		
		else
		{
			
			for(double x = margin+5; x < panelSize-margin+5; x+=interval) {
				for(double y = margin+5; y < panelSize-margin+5; y+=interval)
				{
					int randomX = ThreadLocalRandom.current().nextInt(-12, 12 + 1);
					int randomY = ThreadLocalRandom.current().nextInt(-12, 12 + 1);
					Color pColor = Color.random();
					vM[i][j] = new Vertex3D(x+randomX, y-1+randomY, 0, pColor);
					j++;
				}
				
			i++;
			j=0;
			}
		}
		
		for(int m = 0; m<9;m++)
		{
			for(int n = 0; n<9;n++)
			{
				Polygon poly1 = Polygon.make(vM[m][n], vM[m+1][n], vM[m][n+1]);
				Polygon poly2 = Polygon.make(vM[m+1][n+1], vM[m+1][n], vM[m][n+1]);
				renderer.drawPolygon(poly1, panel);
				renderer.drawPolygon(poly2, panel); 
			}
		}
	}
}
