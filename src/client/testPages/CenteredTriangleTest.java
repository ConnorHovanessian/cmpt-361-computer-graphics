package client.testPages;

import java.util.concurrent.ThreadLocalRandom;

import geometry.Vertex3D;
import polygon.Polygon;
import polygon.PolygonRenderer;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class CenteredTriangleTest {
	private final PolygonRenderer renderer;
	private final Drawable panel;
	private final int panelSize = 650;
	private final int center = panelSize/2;
	private final double maxRotation = 2.0944;
	private final int radius = 275;
	
	public CenteredTriangleTest(Drawable panel, PolygonRenderer renderer) {
		this.panel = panel;
		this.renderer = renderer;
		render();
	}
	
	private void render() {
		Color[] color = new Color[6];
		for(int i = 0; i < 6; i++) {
			double v = (1 - i*.15);
			color[i] = new Color(v, v, v);
		}
		
		for(int i = 0; i < 6; i++) {
			//Calculate triangle
			
			//Base Triangle
			double x1 = center;
			double x2 = center + 238;
			double x3 = center - 238;
			double y1 = center + 275;
			double y2 = center - 137.5;
			double y3 = center - 137.5;
			
			//Translate triangle to origin
			x1 -= center;
			x2 -= center;
			x3 -= center;
			y1 -= center;
			y2 -= center;
			y3 -= center;
			
			//Apply Rotation
			double theta = ThreadLocalRandom.current().nextDouble(0, maxRotation);
			double c = Math.cos(theta);
			double s = Math.sin(theta);
			double px1 = x1*c - y1*s;
			double px2 = x2*c - y2*s;
			double px3 = x3*c - y3*s;
			double py1 = y1*c + x1*s;
			double py2 = y2*c + x2*s;
			double py3 = y3*c + x3*s;
			
			//Translate triangle back
			px1 += center;
			px2 += center;
			px3 += center;
			py1 += center;
			py2 += center;
			py3 += center;
			
			double check = Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
			//Create points, polygon, and draw
			int randomZ = ThreadLocalRandom.current().nextInt(-199, -1 + 1);
			Vertex3D p1 = new Vertex3D(px1, py1, randomZ, color[i]);
			Vertex3D p2 = new Vertex3D(px2, py2, randomZ, color[i]);
			Vertex3D p3 = new Vertex3D(px3, py3, randomZ, color[i]);
			Polygon poly = Polygon.make(p1, p2, p3);
			renderer.drawPolygon(poly, panel);
		}

	
	}
}