//The test to display on the third page is the �random� test. Draw 30 random lines (generate the two endpoints
//uniformly and independently on the interval from 0 to 299 in x and in y) with a random color for each line. (Your line
//renderers must therefore be capable of handling the color of a line: simply multiply the color�s R, G, and B by the
//pixel coverage (which is 1 for DDA and Bresenham�s) and add this to (1 � pixel�s coverage)*(the color that�s already
//in the pixel). Use the same random endpoints and colors for all four panels. I don�t care how you randomly
//choose the colors. 

package client.testPages;

import java.util.concurrent.ThreadLocalRandom;

import geometry.Vertex3D;
import line.LineRenderer;
import windowing.drawable.Drawable;
import windowing.graphics.Color;
import client.Client;

public class RandomLineTest {
	private final LineRenderer renderer;
	private final Drawable panel;
	private final int max = 299;
	private final int min = 0;
	
	
	
	public RandomLineTest(Drawable panel, LineRenderer renderer) {
		this.panel = panel;
		this.renderer = renderer;
		

		if(Client.RandomLineTestFlag != true)
		{
			Client.RandomLineTestFlag = true;
			for(int i=0;i<30;i++)
			{
				//Use max+1 since the bound is exclusive, select all random endpoints before rendering
				Client.pointArrayAx[i] = ThreadLocalRandom.current().nextInt(min, max+1);
				Client.pointArrayAy[i] = ThreadLocalRandom.current().nextInt(min, max+1);
				Client.pointArrayBx[i] = ThreadLocalRandom.current().nextInt(min, max+1);
				Client.pointArrayBy[i] = ThreadLocalRandom.current().nextInt(min, max+1);
				Client.colorArray[i] = Color.random();
				
//				Select colors using max of 255 rather than 300, pack color with bit operations
//			    int red = ThreadLocalRandom.current().nextInt(min, cmax+1);
//			    int g = ThreadLocalRandom.current().nextInt(min, cmax+1);
//			    int b = ThreadLocalRandom.current().nextInt(min, cmax+1);
//			    int color = (0xff << 24) + (red << 16) + (g << 8) + b;
			    
			}
		}
		
		render();
	}
	
	
	private void render() {		
		
		for(int i = 0; i <30; i++)
		{
			Vertex3D pointA = new Vertex3D(Client.pointArrayAx[i],Client.pointArrayAy[i],0, Client.colorArray[i]);
			Vertex3D pointB = new Vertex3D(Client.pointArrayBx[i],Client.pointArrayBy[i],0, Client.colorArray[i]);
			
			renderer.drawLine(pointA, pointB, panel);
		}
	}
}
