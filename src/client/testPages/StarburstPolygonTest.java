package client.testPages;

import geometry.Vertex3D;
import line.LineRenderer;
import polygon.Polygon;
import polygon.PolygonRenderer;
import polygon.Shader;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class StarburstPolygonTest {
	
	private static final int NUM_RAYS = 90;
	private static final double FRACTION_OF_PANEL_FOR_DRAWING = 0.9;
	
	private final PolygonRenderer renderer;
	private final Drawable panel;
	private Vertex3D OldRadialPoint2;
	Vertex3D center;
	
	public StarburstPolygonTest(Drawable panel, PolygonRenderer renderer) {
		this.panel = panel;
		this.renderer = renderer;
		
		makeCenter();
		OldRadialPoint2 = radialPoint(computeRadius(), 0.0);
		render();
	}
	
	private void render() {		
		double radius = computeRadius();
		double angleDifference = (2.0 * Math.PI) / NUM_RAYS;
		double angle = 0.0;
		
		for(int ray = 0; ray < NUM_RAYS; ray++) {
			Color pColor = Color.random();
			center = center.replaceColor(pColor);
			Vertex3D radialPoint1 = OldRadialPoint2;
			radialPoint1 = radialPoint1.replaceColor(pColor);
			angle = angle + angleDifference;
			Vertex3D radialPoint2 = radialPoint(radius, angle);
			radialPoint2 = radialPoint2.replaceColor(pColor);
			
			//By using the previous radialPoint2 as the new radialPoint1, we can avoid calculating each radialPoint twice
			OldRadialPoint2 = radialPoint2;
			
			Polygon poly = Polygon.make(center, radialPoint1, radialPoint2);
			renderer.drawPolygon(poly, panel);
			
		}
	}
	
	private void makeCenter() {
		int centerX = panel.getWidth() / 2;
		int centerY = panel.getHeight() / 2;
		center = new Vertex3D(centerX, centerY, 0, Color.WHITE);
	}
	
	private Vertex3D radialPoint(double radius, double angle) {
		double x = center.getX() + radius * Math.cos(angle);
		double y = center.getY() + radius * Math.sin(angle);
		return new Vertex3D(x, y, 0, Color.WHITE);
	}
	
	private double computeRadius() {
		int width = panel.getWidth();
		int height = panel.getHeight();
		
		int minDimension = width < height ? width : height;
		
		return (minDimension / 2.0) * FRACTION_OF_PANEL_FOR_DRAWING;
	}
}
