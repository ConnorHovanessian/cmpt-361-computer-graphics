package client.testPages;

import java.util.concurrent.ThreadLocalRandom;

import geometry.Vertex3D;
import polygon.Polygon;
import polygon.PolygonRenderer;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class RandomPolygonTest {
	private final PolygonRenderer renderer;
	private final Drawable panel;
	
//	In the fourth panel, generate 20 random triangles (endpoints chosen randomly and uniformly from [0, 299]
//	in both x and y) and render them. They will overlap; this is okay. 
//	int randomX = ThreadLocalRandom.current().nextInt(-12, 12 + 1);
	
	private final int numPoly = 4;
	
	public RandomPolygonTest(Drawable panel, PolygonRenderer renderer) {
		this.panel = panel;
		this.renderer = renderer;

		render();
	}
	
	private void render() {		
		for(int i = 0; i<numPoly;i++) //numPoly triangles
		{
			Vertex3D[] vertices = new Vertex3D[3];
			for(int j=0;j<3;j++) // Three vertices per triangle
			{
				int x = ThreadLocalRandom.current().nextInt(0, panel.getWidth() + 1);
				int y = ThreadLocalRandom.current().nextInt(0, panel.getHeight() + 1);
				Color color = Color.random();
				Vertex3D v = new Vertex3D(x, y, 0.0, color);
				vertices[j] = v;
			}
			Polygon poly = Polygon.make(vertices[0], vertices[1], vertices[2]);
			renderer.drawPolygon(poly, panel);
		}
		
		
	}
}
