package client;

import client.testPages.StarburstLineTest;
import geometry.Point2D;
import line.AlternatingLineRenderer;
import line.ExpensiveLineRenderer;
import line.LineRenderer;
import client.interpreter.SimpInterpreter;
import client.testPages.CenteredTriangleTest;
import client.testPages.MeshPolygonTest;
import client.testPages.ParallelogramTest;
import client.testPages.RandomLineTest;
import client.testPages.RandomPolygonTest;
import client.testPages.StarburstPolygonTest;
import line.AntialiasingLineRenderer;
import line.BresenhamLineRenderer;
import line.DDALineRenderer;
import polygon.FilledPolygonRenderer;
import polygon.WireframePolygonRenderer;
import polygon.PolygonRenderer;
import windowing.PageTurner;
import windowing.drawable.Drawable;
import windowing.drawable.GhostWritingDrawable;
import windowing.drawable.InvertedYDrawable;
import windowing.drawable.PixelClippingDrawable;
import windowing.drawable.TranslatingDrawable;
import windowing.drawable.ZBufferDrawable;
import windowing.drawable.ColoredDrawable;
import windowing.drawable.DepthBufferDrawable;
import windowing.graphics.Color;
import windowing.graphics.Dimensions;

public class Client implements PageTurner {
	private static final int ARGB_WHITE = 0xff_ff_ff_ff;
	private static final int ARGB_GREEN = 0xff_00_ff_40;
	
	private static final int NUM_PAGES = 8;
	protected static final double GHOST_COVERAGE = 0.14;

	private static final int NUM_PANELS = 1;
	private static final Dimensions PANEL_SIZE = new Dimensions(650, 650);
	private static final Point2D[] lowCornersOfPanels = {
			new Point2D( 50, 50)
	};
	
	private final Drawable drawable;
	private int pageNumber = 0;
	
	private Drawable image;
	private Drawable[] panels;
	private Drawable[] ghostPanels;					// use transparency and write only white
	private Drawable largePanel;
	private Drawable fullPanel;
	
	private LineRenderer lineRenderers[];
	private PolygonRenderer polygonRenderer;
	private PolygonRenderer wireframeRenderer;
			
	public static String parameters;
	
	//Arrays declared here for RandomLineTest to keep it the same across panels
	public static boolean RandomLineTestFlag = false;
	public static int pointArrayAx[] = new int[30];
	public static int pointArrayAy[] = new int[30]; 
	public static int pointArrayBx[] = new int[30];
	public static int pointArrayBy[] = new int[30];
	public static Color colorArray[] = new Color [30];
	
	public Client(Drawable drawable) {
		this.drawable = drawable;
		createDrawables();
		createRenderers();
	}
	
//	
//	From Tom's Notes
//	
	
//	@Override
//	public void nextPage() {
//		if(hasArgument) {
//			argumentNextPage();
//		}
//		else {
//			noArgumentNextPage();
//		}
//	}
//
//	private void argumentNextPage() {
//		image.clear();
//		fullPanel.clear();
//		
//		interpreter = new SimpInterpreter(filename + ".simp", fullPanel, renderers);
//		interpreter.interpret();
//	}
//	
//	public void noArgumentNextPage() {
//		System.out.println("PageNumber " + (pageNumber + 1));
//		pageNumber = (pageNumber + 1) % NUM_PAGES;
//		
//		image.clear();
//		fullPanel.clear();
//		String filename;
//
//		switch(pageNumber) {
//		case 1:  filename = "pageA";	 break;
//		case 2:  filename = "pageB";	 break;
//		case 3:	 filename = "pageC";	 break;
//		case 4:  filename = "pageD";	 break;
//		case 5:  filename = "pageE";	 break;
//		case 6:  filename = "pageF";	 break;
//		case 7:  filename = "pageG";	 break;
//		case 8:  filename = "pageH";	 break;
//		case 9:  filename = "pageI";	 break;
//		case 0:  filename = "tomsPageJ";	 break;
//
//		default: defaultPage();
//				 return;
//		}				 
//		interpreter = new SimpInterpreter(filename + ".simp", fullPanel, renderers);
//		interpreter.interpret();
//	}


	public void createDrawables() {
		image = new InvertedYDrawable(drawable);
		image = new TranslatingDrawable(image, point(0, 0), dimensions(750, 750));
		image = new ColoredDrawable(image, ARGB_WHITE);
		largePanel = new TranslatingDrawable(image, point(  50, 50),  dimensions(650, 650));
		image = new PixelClippingDrawable(image);
		image = new ZBufferDrawable(image);
		
		
//		image = new DepthBufferDrawable(image);
		//image = new ColoredDrawable(image, ARGB_GREEN); <- ColoredDrawable is working
		

		fullPanel = new TranslatingDrawable(image, point(  50, 50),  dimensions(650, 650));
		
		
		createPanels();
		createGhostPanels();
	}

	public void createPanels() {
		panels = new Drawable[NUM_PANELS];
		
		for(int index = 0; index < NUM_PANELS; index++) {
			panels[index] = new TranslatingDrawable(image, lowCornersOfPanels[index], PANEL_SIZE);
		}
	}

	private void createGhostPanels() {
		ghostPanels = new Drawable[NUM_PANELS];
		
		for(int index = 0; index < NUM_PANELS; index++) {
			Drawable drawable = panels[index];
			ghostPanels[index] = new GhostWritingDrawable(drawable, GHOST_COVERAGE);
		}
	}
	private Point2D point(int x, int y) {
		return new Point2D(x, y);
	}	
	private Dimensions dimensions(int x, int y) {
		return new Dimensions(x, y);
	}
	private void createRenderers() {
		
		lineRenderers = new LineRenderer[4];
		lineRenderers[0] = DDALineRenderer.make();
		lineRenderers[1] = BresenhamLineRenderer.make();
		lineRenderers[2] = AlternatingLineRenderer.make();
		lineRenderers[3] = AntialiasingLineRenderer.make();
		
		polygonRenderer = FilledPolygonRenderer.make();
		wireframeRenderer = WireframePolygonRenderer.make();
	}
	
//	@Override
//	public void nextPage() {
//		System.out.println("PageNumber " + (pageNumber + 1));
//		pageNumber = (pageNumber + 1) % NUM_PAGES;
//		
//		image.clear();
//		largePanel.clear();
//		switch(pageNumber) {
//		case 1:  wireframePolygonPage(panels);
//				 break;
//		case 2:  filledPolygonPage(panels);
//				 break;
//		case 3:	 centeredTrianglePage(panels);
//				 break;
//		case 4:  wireframePolygonPage(panels);
//				 break;
//		case 0:	 wireframePolygonPage(ghostPanels);		// will be fifth page.  5 == 0 (mod 5)
//				 break;
//		default: defaultPage();
//				 break;
//		}
//	}
	
	@Override
	public void nextPage() {
		if(Main.parameters.length()>0)
		{
			nextPageDefault();
		}
		else
		{
			nextPageNew();
		}
	}
	
	public void nextPageDefault() {
		Drawable depthCueingDrawable;
		SimpInterpreter interpreter;
		RendererTrio renderers = new RendererTrio();
		
		image.clear();
		fullPanel.clear();

		depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE);
		String filename = Main.parameters + ".simp";
		interpreter = new SimpInterpreter(filename, depthCueingDrawable, renderers);
		interpreter.interpret();
	}
	
	public void nextPageNew() {
		Drawable depthCueingDrawable;
		System.out.println("PageNumber " + (pageNumber + 1));
		pageNumber = (pageNumber + 1) % 10;
		SimpInterpreter interpreter;
		RendererTrio renderers = new RendererTrio();
		
		image.clear();
		fullPanel.clear();

		switch(pageNumber) {
		case 1:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageA.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;
//		case 1:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
//		 		 interpreter = new SimpInterpreter("pageA.simp", depthCueingDrawable, renderers);
//		 		 interpreter.interpret();
//				 break;
		case 2:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageB.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;
		case 3:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageC.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		case 4:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageD.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		case 5:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageE.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		case 6:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageF.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		case 7:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageG.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		case 8:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageH.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;
				 
		case 9:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("pageI.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;
				 
		case 0:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE); 
				 interpreter = new SimpInterpreter("connorsPageJ.simp", fullPanel, renderers);
				 interpreter.interpret();
				 break;

		default: defaultPage();
				 break;
		}
	}
	
	public void nextPageOld() {
		Drawable depthCueingDrawable;
		System.out.println("PageNumber " + (pageNumber + 1));
		pageNumber = (pageNumber + 1) % NUM_PAGES;
		SimpInterpreter interpreter;
		RendererTrio renderers = new RendererTrio();
		
		image.clear();
		fullPanel.clear();

		switch(pageNumber) {
		case 1:  new MeshPolygonTest(fullPanel, wireframeRenderer, true);
				 break;
		case 2:  new MeshPolygonTest(fullPanel, polygonRenderer, true);
				 break;
		case 3:	 new CenteredTriangleTest(fullPanel, polygonRenderer);
				 break;

		case 4:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.GREEN); 
				 interpreter = new SimpInterpreter("connorsPage4.simp", depthCueingDrawable, renderers);
				 interpreter.interpret();
				 break;

		case 5:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.RED);
				 interpreter = new SimpInterpreter("connorsPage5.simp", depthCueingDrawable, renderers);
				 interpreter.interpret();
				 break;

		case 6:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE);
				 interpreter = new SimpInterpreter("page6.simp", depthCueingDrawable, renderers);
				 interpreter.interpret();
				 break;		

		case 7:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE);
				 interpreter = new SimpInterpreter("page7.simp", depthCueingDrawable, renderers);
				 interpreter.interpret();
				 break;	

		case 0:  depthCueingDrawable = new DepthBufferDrawable(fullPanel, 0, -200, Color.WHITE);
				 interpreter = new SimpInterpreter("page8.simp", depthCueingDrawable, renderers);
				 interpreter.interpret();
				 break;	

		default: defaultPage();
				 break;
		}
	}

	@FunctionalInterface
	private interface TestPerformer {
		public void perform(Drawable drawable, LineRenderer renderer);
	}
	private void lineDrawerPage(TestPerformer test) {
		image.clear();

		for(int panelNumber = 0; panelNumber < panels.length; panelNumber++) {
			panels[panelNumber].clear();
			test.perform(panels[panelNumber], lineRenderers[panelNumber]);
		}
	}
	public void wireframePolygonPage(Drawable[] panelArray) {
		image.clear();
		for(Drawable panel: panels) {		// 'panels' necessary here.  Not panelArray, because clear() uses setPixel.
			panel.clear();
		}
		new MeshPolygonTest(panelArray[0], wireframeRenderer, true); //Perturbation true
	}
	
	public void filledPolygonPage(Drawable[] panelArray) {
		image.clear();
		for(Drawable panel: panels) {		// 'panels' necessary here.  Not panelArray, because clear() uses setPixel.
			panel.clear();
		}
		new MeshPolygonTest(panelArray[0], polygonRenderer, true); //Perturbation true
	}
	
	public void centeredTrianglePage(Drawable[] panelArray) {
		image.clear();
		for(Drawable panel: panels) {		// 'panels' necessary here.  Not panelArray, because clear() uses setPixel.
			panel.clear();
		}
		new CenteredTriangleTest(panelArray[0], polygonRenderer); 
	}

	private void defaultPage() {
		image.clear();
		largePanel.fill(ARGB_GREEN, Double.MAX_VALUE);
	}
}
