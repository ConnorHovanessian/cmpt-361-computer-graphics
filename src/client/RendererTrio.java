package client;

import line.DDALineRenderer;
import line.LineRenderer;
import polygon.FilledPolygonRenderer;
import polygon.PolygonRenderer;
import polygon.WireframePolygonRenderer;

public class RendererTrio {
	private LineRenderer lineRenderer;
	private PolygonRenderer polygonRenderer;
	private PolygonRenderer wireframeRenderer;
	
	public RendererTrio(){
		lineRenderer = DDALineRenderer.make();
		polygonRenderer = FilledPolygonRenderer.make();
		wireframeRenderer = WireframePolygonRenderer.make();
	}

	public LineRenderer getLineRenderer() {
		return lineRenderer;
	}

	public PolygonRenderer getFilledRenderer() {
		return polygonRenderer;
	}

	public PolygonRenderer getWireframeRenderer() {
		return wireframeRenderer;
	}
}
