package client.interpreter;

import java.util.ArrayList;
import java.util.List;
import geometry.Point3DH;
import geometry.Vertex3D;
import polygon.Polygon;
import windowing.graphics.Color;

class ObjReader {
	private static final char COMMENT_CHAR = '#';
	private static final int NOT_SPECIFIED = -1;

	public class ObjVertex {
		int	vertIndex;
		int texIndex;
		int normIndex;
		
		public ObjVertex(int vertIndex, int texIndex, int normIndex){
			this.vertIndex = vertIndex;
			this.texIndex = texIndex;
			this.normIndex = normIndex;
		}
		
		public void setVertexIndex(int vertIndex) {
			this.vertIndex = vertIndex;
		} 
		public void setTextureIndex(int texIndex) {
			this.texIndex = texIndex;
		}
		public void setNormalIndex(int normIndex) {
			this.normIndex = normIndex;
		}
		public int getVertexIndex() {
			return vertIndex;
		}
		public int getTextureIndex() {
			return texIndex;
		}
		public int getNormalIndex() {
			return normIndex;
		}	
		
	}
	public class ObjFace extends ArrayList<ObjVertex> {
		private static final long serialVersionUID = -4130668677651098160L;
	}	
	private LineBasedReader reader;
	
	private List<Vertex3D> objVertices;
	private List<Vertex3D> transformedVertices;
	private List<Point3DH> objNormals;
	private List<ObjFace> objFaces;

	private Color defaultColor;
	
	ObjReader(String filename, Color defaultColor) {
		reader = new LineBasedReader(filename);
		this.defaultColor = defaultColor;
		objVertices = new ArrayList<Vertex3D>();
		transformedVertices = new ArrayList<Vertex3D>();
		objNormals = new ArrayList<Point3DH>();
		objFaces = new ArrayList<ObjFace>();
	}

	public void render(SimpInterpreter simpInterpreter) {
		// : Implement.  All of the vertices, normals, and faces have been defined.
		// First, transform all of the vertices.		
		// Then, go through each face, break into triangles if necessary, and send each triangle to the renderer.
		// You may need to add arguments to this function, and/or change the visibility of functions in SimpInterpreter.
		for(ObjFace face: objFaces) {
			Polygon polygon = polygonForFace(face);
			for(int i = 0; i + 2 < polygon.length(); i++) {
				simpInterpreter.polygon(polygon.get(0),polygon.get(i+1),polygon.get(i+2));
			}
		}
	}
	
	private Polygon polygonForFace(ObjFace face) {
		// : This function might be used in render() above.  Implement it if you find it handy.
		Polygon ret = Polygon.makeEmpty();
		for(ObjVertex objVertex: face) {
			//: make the vertex corresponding to this objVertex
			if(objVertex.getVertexIndex() < objVertices.size() ){
				Vertex3D vertex = objVertices.get(objVertex.getVertexIndex());
				ret.add(vertex);
			}
		}
		return ret;
	}

	public void read() {
		while(reader.hasNext() ) {
			String line = reader.next().trim();
			interpretObjLine(line);
		}
	}
	private void interpretObjLine(String line) {
		if(!line.isEmpty() && line.charAt(0) != COMMENT_CHAR) {
			String[] tokens = line.split("[ \t,()]+");
			if(tokens.length != 0) {
				interpretObjCommand(tokens);
			}
		}
	}

	private void interpretObjCommand(String[] tokens) {
		switch(tokens[0]) {
		case "v" :
		case "V" :
			interpretObjVertex(tokens);
			break;
		case "vn":
		case "VN":
			interpretObjNormal(tokens);
			break;
		case "f":
		case "F":
			interpretObjFace(tokens);
			break;
		default:	//nothing
			break;
		}
	}
	private void interpretObjFace(String[] tokens) {
		ObjFace face = new ObjFace();
		
		for(int i = 1; i<tokens.length; i++) {
			String token = tokens[i];
			String[] subtokens = token.split("/");
			
			int vertIndex  = objIndex(subtokens, 0, objVertices.size());
			int textureIndex = objIndex(subtokens, 1, 0);
			int normalIndex  = objIndex(subtokens, 2, objNormals.size());
			ObjVertex vert = new ObjVertex(vertIndex,textureIndex,normalIndex);
			face.add(vert);
		}
		objFaces.add(face);
	}

	private int objIndex(String[] subtokens, int tokenIndex, int baseForNegativeIndices) {
		// : write this.  subtokens[tokenIndex], if it exists, holds a string for an index.
		// use Integer.parseInt() to get the integer value of the index.
		// Be sure to handle both positive and negative indices.

		if(subtokens.length<=tokenIndex) //Checking to see if it even exists
			return 0;
		int objIndex = Integer.parseInt(subtokens[tokenIndex]);
		if(objIndex < 0) {
			return baseForNegativeIndices + objIndex;
		}
		return objIndex;
	}
	
	private void interpretObjNormal(String[] tokens) {
		int numArgs = tokens.length - 1;
		if(numArgs != 3) {
			throw new BadObjFileException("vertex normal with wrong number of arguments : " + numArgs + ": " + tokens);				
		}
		Point3DH normal = SimpInterpreter.interpretPoint(tokens, 1);
		objNormals.add(normal);
	}
	private void interpretObjVertex(String[] tokens) {
		int numArgs = tokens.length - 1;
		Point3DH point = objVertexPoint(tokens, numArgs);
		Color color = objVertexColor(tokens, numArgs);
		Vertex3D vert = new Vertex3D(point,color);
		objVertices.add(vert);
	}

	private Color objVertexColor(String[] tokens, int numArgs) {
		if(numArgs == 6) {
			return SimpInterpreter.interpretColor(tokens, 4);
		}
		if(numArgs == 7) {
			return SimpInterpreter.interpretColor(tokens, 5);
		}
		return defaultColor;
	}

	private Point3DH objVertexPoint(String[] tokens, int numArgs) {
		if(numArgs == 3 || numArgs == 6) {
			return SimpInterpreter.interpretPoint(tokens, 1);
		}
		else if(numArgs == 4 || numArgs == 7) {
			return SimpInterpreter.interpretPointWithW(tokens, 1);
		}
		throw new BadObjFileException("vertex with wrong number of arguments : " + numArgs + ": " + tokens);
	}
}