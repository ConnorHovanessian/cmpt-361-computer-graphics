package client.interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import client.interpreter.LineBasedReader;
import client.interpreter.SimpInterpreter.RenderStyle;
import geometry.Point3DH;
import geometry.Rectangle;
import geometry.Vertex3D;
import javafx.geometry.Point3D;
import line.LineRenderer;
import polygon.Clipper;
import windowing.drawable.DepthBufferDrawable;
import client.RendererTrio;
import geometry.Transformation;
import polygon.Polygon;
import polygon.PolygonRenderer;
import polygon.Shader;
import windowing.drawable.Drawable;
import windowing.graphics.Color;
import windowing.graphics.Dimensions;

public class SimpInterpreter {
	private static final int NUM_TOKENS_FOR_POINT = 3;
	private static final int NUM_TOKENS_FOR_COMMAND = 1;
	private static final int NUM_TOKENS_FOR_COLORED_VERTEX = 6;
	private static final int NUM_TOKENS_FOR_UNCOLORED_VERTEX = 3;
	private static final char COMMENT_CHAR = '#';
	private RenderStyle renderStyle;

	private Transformation CTM;
	private Transformation inverseCTM;
	private Transformation perspectiveTransformation;
	private Stack<Transformation> matrixStack;
	private Stack<Transformation> inverseMatrixStack;
	private Transformation worldToScreen;

	private static int WORLD_LOW_X = -100;
	private static int WORLD_HIGH_X = 100;
	private static int WORLD_LOW_Y = -100;
	private static int WORLD_HIGH_Y = 100;

	private LineBasedReader reader;
	private Stack<LineBasedReader> readerStack;
	private Shader ambientShader;

	private Color defaultColor = Color.WHITE;
	private Color ambientLight = Color.BLACK;

	private Drawable drawable;
	private Drawable depthCueingDrawable;

	private LineRenderer lineRenderer;
	private PolygonRenderer filledRenderer;
	private PolygonRenderer wireframeRenderer;
	private Transformation cameraToScreen;
	private Clipper clipper;

	private List<Vertex3D> vertList = new ArrayList<Vertex3D>();
	private List<Polygon> polyList = new ArrayList<Polygon>();
	private List<List<Vertex3D>> vertListList = new ArrayList<List<Vertex3D>>();

	private double xlow;
	private double ylow;
	private double xhigh;
	private double yhigh;
	private double hither;
	private double yon;

	public enum RenderStyle {
		FILLED, WIREFRAME;
	}

	public SimpInterpreter(String filename, Drawable drawable, RendererTrio renderers) {
		this.drawable = drawable;
		this.depthCueingDrawable = drawable;
		this.lineRenderer = renderers.getLineRenderer();
		this.filledRenderer = renderers.getFilledRenderer();
		this.wireframeRenderer = renderers.getWireframeRenderer();
		this.defaultColor = Color.WHITE;
		makeWorldToScreenTransform(drawable.getDimensions());
		this.clipper = new Clipper(drawable);
		
		String[] ambiTokens = {"ambient", "1", "1", "1"};
		interpretAmbient(ambiTokens);

		reader = new LineBasedReader(filename);
		readerStack = new Stack<>();
		renderStyle = RenderStyle.FILLED;

		CTM = Transformation.identity();
		inverseCTM = Transformation.identity();
		matrixStack = new Stack<>();
		inverseMatrixStack = new Stack<>();
	}

	private void makeWorldToScreenTransform(Dimensions dimensions) {
		// Go from world to window, then window to screen
		// World to window, we just drop z axis
		int width = dimensions.getWidth();
		int height = dimensions.getHeight();
		// Window to screen
		double scale = (double) (width / 200.0);
		worldToScreen = Transformation.identity();
		Transformation tScale = Transformation.scale(scale, scale, 1.0);
		Transformation tTranslate = Transformation.translate(325.0, 325.0, 0.0);
		worldToScreen = Transformation.multiply(tTranslate, tScale);
	}

	public void interpret() {
		while (reader.hasNext()) {
			String line = reader.next().trim();
			interpretLine(line);
			while (!reader.hasNext()) {
				if (readerStack.isEmpty()) {
					return;
				} else {
					reader = readerStack.pop();
				}
			}
		}
	}

	public void interpretLine(String line) {
		if (!line.isEmpty() && line.charAt(0) != COMMENT_CHAR) {
			String[] tokens = line.split("[ \t,()]+");
			if (tokens.length != 0) {
				interpretCommand(tokens);
			}
		}
	}

	private void interpretCommand(String[] tokens) {
		switch(tokens[0]) {
		case "{" :      push();   break;
		case "}" :      pop();    break;
		case "wire" :   wire();   break;
		case "filled" : filled(); break;
		
		case "file" :		interpretFile(tokens);		break;
		case "scale" :		interpretScale(tokens);		break;
		case "translate" :	interpretTranslate(tokens);	break;
		case "rotate" :		interpretRotate(tokens);	break;
		case "line" :		interpretLine(tokens);		break;
		case "polygon" :	interpretPolygon(tokens);	break;
		case "camera" :		interpretCamera(tokens);	break;
		case "surface" :	interpretSurface(tokens);	break;
		case "ambient" :	interpretAmbient(tokens);	break;
		case "depth" :		interpretDepth(tokens);		break;
		case "obj" :		interpretObj(tokens);		break;
		
		default :
			System.err.println("bad input line: " + tokens);
			break;
		}
	}

	private void interpretObj(String[] tokens) {
		String objFilename = tokens[1];
		objFilename = objFilename.substring(1, objFilename.length() - 1);
		objFile(objFilename + ".obj");
	}

	private void interpretDepth(String[] tokens) {
		double near = cleanNumber(tokens[1]);
		double far = cleanNumber(tokens[2]);
		double r = cleanNumber(tokens[3]);
		double g = cleanNumber(tokens[4]);
		double b = cleanNumber(tokens[5]);
		Color farColor = new Color(r, g, b);
		depthCueingDrawable = new DepthBufferDrawable(drawable, (int) near, (int) far, farColor);
	}

	// camera <xlow> <ylow> <xhigh> <yhigh> <hither> <yon>
	private void interpretCamera(String[] tokens) {
		// Read tokens and set clipper
		xlow = cleanNumber(tokens[1]);
		ylow = cleanNumber(tokens[2]);
		xhigh = cleanNumber(tokens[3]);
		yhigh = cleanNumber(tokens[4]);
		hither = cleanNumber(tokens[5]);
		yon = cleanNumber(tokens[6]);
		clipper.setWindow(xlow, ylow, xhigh, yhigh, hither, yon);

		// Set a worldToCamera transformation to the inverse of the current CTM
		Stack<Transformation> tempMatrixStack = new Stack<>();
		int matrixSize = matrixStack.size();
		// Pop through the whole matrix to premultiply by inverseCTM when camera is
		// called
		// Multiply worldToCamera into the CTM and everything on the matrix stack
		Transformation worldToCamera = inverseCTM;
		for (int i = 0; i < matrixSize; i++) {
			Transformation t = matrixStack.pop();
			t = Transformation.multiply(worldToCamera, t);
			tempMatrixStack.push(t);
		} // pop the temp stack to add transformations back into the original
		for (int i = 0; i < matrixSize; i++)
			matrixStack.push(tempMatrixStack.pop());
		CTM = Transformation.multiply(CTM, worldToCamera);
		// create a projectedToScreen transformation based on the window (first four
		// numbers from tokens) and the size of the drawable (0 to 650).
		if (xhigh + (0 - xlow) == yhigh + (0 - ylow)) { // If square
			Dimensions dimensions = drawable.getDimensions();
			double xScale = xhigh + (0 - xlow);
			double yScale = yhigh + (0 - ylow);

			Transformation tScale = Transformation.scale(dimensions.getWidth() / xScale,
					dimensions.getHeight() / yScale, 1);
			Transformation tTranslate = Transformation.translate(0 - (xlow), 0 - ylow, 0.0);
			Transformation projectedToScreen = Transformation.multiply(tScale, tTranslate);

			cameraToScreen = Transformation.multiply(projectedToScreen, Transformation.identity());
		} else { // If not square -- assumes wider than it is tall
			Dimensions dimensions = drawable.getDimensions();
			double xScale = xhigh + (0 - xlow);
			double yScale = yhigh + (0 - ylow);
			double ratio = Math.abs(xScale / yScale);
			double preScaleTranslate = yScale / ratio;

			Transformation tTranslate1 = Transformation.translate(0.0, preScaleTranslate, 0.0); 
			// Elevate to middle of screen
			Transformation tScale = Transformation.scale(dimensions.getWidth() / ratio, dimensions.getHeight()/ ratio, 1);
			Transformation tTranslate2 = Transformation.translate(0 - (xlow), 0 - ylow, 0.0);
			Transformation projectedToScreen = Transformation.multiply(tTranslate1, Transformation.identity());
			projectedToScreen = Transformation.multiply(tScale, tTranslate1);
			projectedToScreen = Transformation.multiply(projectedToScreen, tTranslate2);
			cameraToScreen = Transformation.multiply(projectedToScreen, Transformation.identity());
		}
	}

	private void interpretSurface(String[] tokens) {
		double r = cleanNumber(tokens[1]);
		double g = cleanNumber(tokens[2]);
		double b = cleanNumber(tokens[3]);
		Color surface = new Color(r, g, b);
		defaultColor = surface;
	}

	private void interpretAmbient(String[] tokens) {
		double r = cleanNumber(tokens[1]);
		double g = cleanNumber(tokens[2]);
		double b = cleanNumber(tokens[3]);
		Color ambi = new Color(r, g, b);
		this.ambientLight = ambi;
		this.ambientShader = c -> ambi.multiply(c);
	}

	private void push() {
		matrixStack.push(CTM);
		inverseMatrixStack.push(CTM);
	}

	private void pop() {
		CTM = matrixStack.pop();
		inverseCTM = inverseMatrixStack.pop();
	}

	private void wire() {
		renderStyle = RenderStyle.WIREFRAME;
	}

	private void filled() {
		renderStyle = RenderStyle.FILLED;
	}

	// this one is complete.
	private void interpretFile(String[] tokens) {
		String quotedFilename = tokens[1];
		int length = quotedFilename.length();
		assert quotedFilename.charAt(0) == '"' && quotedFilename.charAt(length - 1) == '"';
		String filename = quotedFilename.substring(1, length - 1);
		file(filename + ".simp");
	}

	private void file(String filename) {
		readerStack.push(reader);
		reader = new LineBasedReader(filename);
	}

	private void interpretScale(String[] tokens) {
		double sx = cleanNumber(tokens[1]);
		double sy = cleanNumber(tokens[2]);
		double sz = cleanNumber(tokens[3]);
		CTM = Transformation.multiply(CTM, Transformation.scale(sx, sy, sz));
		inverseCTM = Transformation.multiply(Transformation.scale(1 / sx, 1 / sy, 1 / sz), inverseCTM);
	}

	private void interpretTranslate(String[] tokens) {
		double tx = cleanNumber(tokens[1]);
		double ty = cleanNumber(tokens[2]);
		double tz = cleanNumber(tokens[3]);
		CTM = Transformation.multiply(CTM, Transformation.translate(tx, ty, tz));
		inverseCTM = Transformation.multiply(Transformation.translate(-tx, -ty, -tz), inverseCTM);
	}

	private void interpretRotate(String[] tokens) {
		String axisString = tokens[1];
		double angleInDegrees = cleanNumber(tokens[2]);
		double angle = Math.toRadians(angleInDegrees);
		CTM = Transformation.multiply(CTM, Transformation.rotate(axisString, angle));
		inverseCTM = Transformation.multiply(Transformation.rotate(axisString, -angle), inverseCTM);
	}

	private static double cleanNumber(String string) {
		return Double.parseDouble(string);
	}

	private enum VertexColors {
		COLORED(NUM_TOKENS_FOR_COLORED_VERTEX), UNCOLORED(NUM_TOKENS_FOR_UNCOLORED_VERTEX);

		private int numTokensPerVertex;

		private VertexColors(int numTokensPerVertex) {
			this.numTokensPerVertex = numTokensPerVertex;
		}

		public int numTokensPerVertex() {
			return numTokensPerVertex;
		}
	}

	private void interpretLine(String[] tokens) {
		Vertex3D[] vertices = interpretVertices(tokens, 2, 1);
		line(vertices[0], vertices[1]);
	}

	private void interpretPolygon(String[] tokens) {
		Vertex3D[] vertices = interpretVertices(tokens, 3, 1);
		polygon(vertices[0], vertices[1], vertices[2]);
	}

	public Vertex3D[] interpretVertices(String[] tokens, int numVertices, int startingIndex) {
		VertexColors vertexColors = verticesAreColored(tokens, numVertices);
		Vertex3D vertices[] = new Vertex3D[numVertices];

		for (int index = 0; index < numVertices; index++) {
			vertices[index] = interpretVertex(tokens, startingIndex + index * vertexColors.numTokensPerVertex(),
					vertexColors);
			// vertices[index] = Transformation.multiply(vertices[index], CTM);
		}
		return vertices;
	}

	public VertexColors verticesAreColored(String[] tokens, int numVertices) {
		return hasColoredVertices(tokens, numVertices) ? VertexColors.COLORED : VertexColors.UNCOLORED;
	}

	public boolean hasColoredVertices(String[] tokens, int numVertices) {
		return tokens.length == numTokensForCommandWithNVertices(numVertices);
	}

	public int numTokensForCommandWithNVertices(int numVertices) {
		return NUM_TOKENS_FOR_COMMAND + numVertices * (NUM_TOKENS_FOR_COLORED_VERTEX);
	}

	private Vertex3D interpretVertex(String[] tokens, int startingIndex, VertexColors colored) {
		Point3DH point = interpretPoint(tokens, startingIndex);

		Color color = defaultColor;
		double x = point.getX();
		double y = point.getY();
		double z = point.getZ();

		if (colored == VertexColors.COLORED) {
			color = interpretColor(tokens, startingIndex + NUM_TOKENS_FOR_POINT);
		}
		Vertex3D ret = new Vertex3D(x, y, z, color);
		return ret;
	}

	public static Point3DH interpretPoint(String[] tokens, int startingIndex) {
		double x = cleanNumber(tokens[startingIndex]);
		double y = cleanNumber(tokens[startingIndex + 1]);
		double z = cleanNumber(tokens[startingIndex + 2]);

		Point3DH point = new Point3DH(x, y, z);
		return point;
	}

	public static Color interpretColor(String[] tokens, int startingIndex) {
		double r = cleanNumber(tokens[startingIndex]);
		double g = cleanNumber(tokens[startingIndex + 1]);
		double b = cleanNumber(tokens[startingIndex + 2]);

		Color color = new Color(r, g, b);
		return color;
	}

	public void line(Vertex3D p1, Vertex3D p2) {
		Vertex3D[] clippedLine = clipper.checkLine(p1, p2);
		Vertex3D screenP1 = transformToCamera(clippedLine[0]);
		Vertex3D screenP2 = transformToCamera(clippedLine[1]);
		
		if ( clippedLine!= null)
			this.lineRenderer.drawLine(screenP1, screenP2, depthCueingDrawable);
	}

	public void polygon(Vertex3D p1, Vertex3D p2, Vertex3D p3) {
		Vertex3D screenP1 = transformToCamera(p1);
		Vertex3D screenP2 = transformToCamera(p2);
		Vertex3D screenP3 = transformToCamera(p3);
		Polygon poly = Polygon.makeEnsuringClockwise(screenP1, screenP2, screenP3);
		vertList = clipper.clipZ(poly);
		if(vertList.size()<3||vertList.size()>4)
			return;
		vertList = applyPerspective(vertList);

		for (int i = 0; i + 2 < vertList.size(); i++) {
			poly = Polygon.makeEnsuringClockwise(vertList.get(0), vertList.get(i + 1), vertList.get(i + 2));
			polyList.add(poly);
		}

		for (int i = 0; i < polyList.size(); i++) {
			vertList = clipper.clipXY(polyList.get(i));
			vertList = applyProjection(vertList);
			vertListList.add(vertList);
		}

		for (int j = 0; j < vertListList.size(); j++) {
			for (int i = 0; i + 2 < vertListList.get(j).size(); i++) {
				Polygon p = Polygon.makeEnsuringClockwise(vertList.get(0), vertList.get(i + 1), vertList.get(i + 2));
				if (renderStyle == RenderStyle.FILLED) 
					filledRenderer.drawPolygon(p, depthCueingDrawable, ambientShader);
				else 
					wireframeRenderer.drawPolygon(p, depthCueingDrawable, ambientShader);
			}
		}

		vertListList.clear();
		polyList.clear();
	}

	private List<Vertex3D> applyPerspective(List<Vertex3D> list) {
		List<Vertex3D> tempVertList = new ArrayList<Vertex3D>();
		double[][] perspectiveMatrix = {{ 1, 0, 0, 0 }, { 0, 1, 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, -1, 0 }};
		for (int i = 0; i < list.size(); i++) {
			Vertex3D v = list.get(i);
			double[] p = new double[4];
			p[0] = v.getX();
			p[1] = v.getY();
			p[2] = v.getZ();
			p[3] = 1;
			p = Transformation.multiply(p, perspectiveMatrix);
			Vertex3D vertex = new Vertex3D(p[0]/p[3],p[1]/p[3],p[2], v.getColor());
			tempVertList.add(vertex);
		}
		return tempVertList;
	}

	private List<Vertex3D> applyProjection(List<Vertex3D> list) {
		List<Vertex3D> tempVertList = new ArrayList<Vertex3D>();
		for (int i = 0; i < vertList.size(); i++) {
			Vertex3D v = vertList.get(i);
			v = Transformation.multiply(v, cameraToScreen);
			tempVertList.add(v);
		}
		return tempVertList;
	}

	private Vertex3D transformToCamera(Vertex3D vertex) {
		vertex = Transformation.multiply(vertex, CTM);
		return vertex;
	}

	private void objFile(String filename) {
		ObjReader objReader = new ObjReader(filename, defaultColor);
		objReader.read();
		objReader.render(this);
	}

	public static Point3DH interpretPointWithW(String[] tokens, int i) {
		double x = cleanNumber(tokens[i]);
		double y = cleanNumber(tokens[i + 1]);
		double z = cleanNumber(tokens[i + 2]);
		double w = cleanNumber(tokens[i + 3]);
		Point3DH p = new Point3DH(x/w, y/w, z/w);
		return p;
	}

}
