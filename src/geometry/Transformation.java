package geometry;

public class Transformation {
	public double[][] matrix = new double[4][4];
	
	public Transformation(double[][] matrix) {
		this.matrix = matrix;
	}
	
	public static Transformation identity() {
		double[][] matrix = new double[4][4];
		for(int i=0;i<4;i++) // Assign all zeros
			for(int j=0;j<4;j++)
				matrix[i][j]=0;
		for(int i=0;i<4;i++) // Assign identity 1s
			matrix[i][i]=1;
		Transformation I = new Transformation(matrix);
		
		return I;
	}

	public static Transformation scale(double scaleX, double scaleY, double scaleZ) {
		Transformation M = Transformation.identity();
		M.matrix[0][0]=1*scaleX;
		M.matrix[1][1]=1*scaleY;
		M.matrix[2][2]=1*scaleZ;
		return M;
	}
	
	public static Transformation rotate(String axis, double angle) {
		
		Transformation M = Transformation.identity();
		double c = Math.cos(angle);
		double s = Math.sin(angle);
		
		switch(axis) {
		case "X": M.matrix[1][1] = c; M.matrix[1][2] = -1*s; M.matrix[2][1] = s; M.matrix[2][2] = c; 	break;
		case "Y": M.matrix[0][0] = c; M.matrix[0][2] = s; M.matrix[2][0] = -1*s; M.matrix[2][2] = c; 	break; 
		case "Z": M.matrix[0][0] = c; M.matrix[0][1] = -1*s; M.matrix[1][0] = s; M.matrix[1][1] = c; 	break;
		}
		return M;
	}

	public static Transformation translate(double translateX, double translateY, double translateZ) {
		Transformation M = Transformation.identity();
		M.matrix[0][3]=translateX;
		M.matrix[1][3]=translateY;
		M.matrix[2][3]=translateZ;
		return M;
	}
	
	//Multiplies t1 by t2
    public static Transformation multiply(Transformation t1, Transformation t2) {
    	double[][] m1 = t1.matrix;
    	double[][] m2 = t2.matrix;
        int m1Col = m1[0].length; 
        int m2Row = m2.length;    
        if(m1Col != m2Row) return null; //Check to see if multiplication will work
        int mRRow = m1.length;    
        int mRCol = m2[0].length; 
        double[][] mRet = new double[mRRow][mRCol];
        for(int i = 0; i < mRRow; i++) {         
            for(int j = 0; j < mRCol; j++) {    
                for(int k = 0; k < m1Col; k++) { 
                    mRet[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        
        Transformation ret = new Transformation(mRet);
        return ret;
    }


	public static Vertex3D multiply(Vertex3D t1, Transformation t2) {
		double[] m1 = new double[4];
		m1[0] = t1.getX();
		m1[1] = t1.getY();
		m1[2] = t1.getZ();
		m1[3] = 1;
    	double[][] m2 = t2.matrix;
    	double[] mRet = new double[4];
    	 
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++){
				mRet[i] += (m2[i][j] * m1[j]);
			}
		}
    	Vertex3D retV = new Vertex3D(mRet[0],mRet[1],mRet[2],t1.getColor());
       
      
        return retV;
	}

	public static double[] multiply(double[] v, double[][] m) {
		double[] t = {0,0,0,0};
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++)
				t[i] = t[i] + (m[i][j] * v[j]);
		}
		return t;
	}

}
