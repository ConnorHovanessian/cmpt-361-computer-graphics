package polygon;
import java.util.ArrayList;
import java.util.List;
import geometry.Vertex3D;
import polygon.Polygon;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class Clipper {
	Polygon polygon;
	Drawable panel;
	double dx, dy, ddx, ddy,  hither, yon;
	int yhigh, ylow, xhigh, xlow, yhigh2, ylow2, xhigh2, xlow2;
	
    private static final int in = 0;
    private static final int left = 1;
    private static final int right = 2;
    private static final int bot = 4;
    private static final int top = 8;
//			left	central	right
//top		1001	1000	1010
//central	0001	0000	0010
//bottom	0101	0100	0110

	public Clipper(Drawable panel) {
		this.panel = panel;
		xlow2 = 0;
		xhigh2 = 650;
		ylow2 = 0;
		yhigh2 = 650;
	}

	public void setWindow(double xlow, double ylow, double xhigh, double yhigh, double hither, double yon) {
		this.xlow = (int) xlow;
		this.xhigh = (int) xhigh;
		this.ylow = (int) ylow;
		this.yhigh = (int) yhigh;
		this.hither = hither;
		this.yon = yon;
	}
	
	public List<Vertex3D> clipZ(Polygon polygon){
		List<Vertex3D> poly = new ArrayList<Vertex3D>();
		for(int vertex = 0; vertex < polygon.length(); vertex++) 
			poly.add(polygon.get(vertex));
		
		poly = yon(poly);
		poly = hither(poly);
		return poly;
	}
	
			//	left	central	right
	//top		1001	1000	1010
	//central	0001	0000	0010
	//bottom	0101	0100	0110
	public Vertex3D[] checkLine(Vertex3D p1, Vertex3D p2) { //Clip line to box in 2d
		double x1 = p1.getX();
		double x2 = p2.getX();
		double y1 = p1.getY();
		double y2 = p2.getY();
		Vertex3D[] clippedLine = new Vertex3D[2];
		int code1 = computeCode(p1);
		int code2 = computeCode(p2);
		while(true)
		{
			if(code1==0 && code2==0) //trivial accept
			{
				clippedLine[0] = p1;
				clippedLine[1] = p2;
				return clippedLine;
			}
			if ((code1&code2) != 0)  //trivial reject
				return null;
			
			double x, y, z; //Our return clippedPoly values
			Color color;
			
			//Choose one of the points that is outside
			int codeOut;
	        if(code1 != 0) 
        	{
	        	codeOut = code1;
	        	z = p1.getZ();
	        	color = p1.getColor();
        	}
	        if(code2 != 0) 
        	{
	        	codeOut = code2;
	        	z = p2.getZ();
	        	color = p2.getColor();
        	}
	        else return null; //This shouldn't be reachable

	        // Find the intersection point for our point 
	        if ((codeOut & top) != 0) {
	            x = x1 + (x2 - x1) * (yhigh - y1) / (y2 - y1);
	            y = yhigh;
	        } if ((codeOut & bot) != 0) {
	            x = x1 + (x2 - x1) * (ylow - y1) / (y2 - y1);
	            y = ylow;
	        } if ((codeOut & right) != 0) {
	            y = y1 + (y2 - y1) * (xhigh - x1) / (x2 - x1);
	            x = xhigh;
	        } else {
	            y = y1 + (y2 - y1) * (xlow - x1) / (x2 - x1);
	            x = xlow;
	        }
	        if (codeOut == code1) { // We know this can't be ambiguous since same opcode ->trivial solution
	            Vertex3D returnVertex = new Vertex3D(x, y, z, color);
	            code1 = computeCode(returnVertex);
	        } else {
	            Vertex3D returnVertex = new Vertex3D(x, y, z, color);
	            code2 = computeCode(returnVertex);
	        }
		}
	}
	
	public int computeCode(Vertex3D v)
	{
		double x = v.getX();
		double y = v.getY();
        int code = in;
        //|= is the bitwise OR operator, but functions like += or -=
        if (x < xlow) code |= left;
        if (x > xhigh) code |= right;
        if (y < ylow) code |= bot;
        if (y > yhigh) code |= top;
        return code;
	}
	
	private List<Vertex3D> hither(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		int lastVertex = 0; //Last vertex we drew
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size()) nextVertex = 0; //To catch last vertex -> first vertex edge
			if(poly.get(vertex).getZ() < hither && poly.get(nextVertex).getZ() < hither) //inside inside
			{
				clippedPoly.add(poly.get(nextVertex));
				lastVertex = nextVertex;
			}
			if(poly.get(vertex).getZ() < hither && poly.get(nextVertex).getZ() >= hither) //inside to outside
			{
				Vertex3D v = triZ(poly.get(vertex), poly.get(nextVertex), hither);
				clippedPoly.add(v);
				lastVertex = nextVertex; //nextVertex represents our clipped v
			}
				
			if(poly.get(vertex).getZ() >= hither && poly.get(nextVertex).getZ() < hither) {  //outside to inside
				Vertex3D v = triZ(poly.get(nextVertex), poly.get(lastVertex), hither);
				clippedPoly.add(v);
				clippedPoly.add(poly.get(nextVertex));
				lastVertex = nextVertex;
			}
//			Outside to outside doesn't get drawn
			nextVertex++;
		}
		return(clippedPoly);
	}
	
	private List<Vertex3D> yon(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		int lastVertex = 0; 
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size()) nextVertex = 0; 
			double z1 = poly.get(vertex).getZ();
			double z2 = poly.get(nextVertex).getZ();
			if(poly.get(vertex).getZ() > yon && poly.get(nextVertex).getZ() > yon) 
			{
				clippedPoly.add(poly.get(nextVertex));
				lastVertex = nextVertex;
			}
			if(poly.get(vertex).getZ() > yon && poly.get(nextVertex).getZ() <= yon) 
			{
				Vertex3D v = triZ(poly.get(vertex), poly.get(nextVertex), yon);
				clippedPoly.add(v);
				lastVertex = nextVertex;
			}
			if(poly.get(vertex).getZ() <= yon && poly.get(nextVertex).getZ() > yon) {
				Vertex3D v = triZ(poly.get(nextVertex), poly.get(vertex), yon);
				clippedPoly.add(v);
				clippedPoly.add(poly.get(nextVertex));
				lastVertex = nextVertex;
			}
			nextVertex++;
		}
		return(clippedPoly);
	}
	
	public List<Vertex3D> clipXY(Polygon polygon) {
		List<Vertex3D> poly = new ArrayList<Vertex3D>();
		for(int vertex = 0; vertex < polygon.length(); vertex++) //Convert polygon into list of vertices
			poly.add(polygon.get(vertex));
		poly = clipClockwise(poly);
		return poly;
	}
	
	private List<Vertex3D> clipClockwise(List<Vertex3D> poly) {
		//Clip clockwise from top
		poly = top(poly);
		poly = right(poly);
		poly = bot(poly);
		poly = left(poly);
		return poly;
	}

	
	private List<Vertex3D> top(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size()) 
				nextVertex = 0;
			if(poly.get(vertex).getY() <= yhigh && poly.get(nextVertex).getY() <= yhigh) 
				clippedPoly.add(poly.get(nextVertex));
			if(poly.get(vertex).getY() <= yhigh && poly.get(nextVertex).getY() > yhigh) 
				clippedPoly.add(triY(poly.get(vertex), poly.get(nextVertex), yhigh));
			if(poly.get(vertex).getY() > yhigh && poly.get(nextVertex).getY() <= yhigh) {
				clippedPoly.add(triY(poly.get(nextVertex), poly.get(vertex), yhigh));
				clippedPoly.add(poly.get(nextVertex));
			}
			nextVertex++;
		}
		return clippedPoly;
	}
	
	private List<Vertex3D> right(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size())
				nextVertex = 0;
			if(poly.get(vertex).getX() <= xhigh && poly.get(nextVertex).getX() <= xhigh)
				clippedPoly.add(poly.get(nextVertex));
			if(poly.get(vertex).getX() <= xhigh && poly.get(nextVertex).getX() > xhigh)
				clippedPoly.add(triX(poly.get(vertex), poly.get(nextVertex), xhigh));
			if(poly.get(vertex).getX() > xhigh && poly.get(nextVertex).getX() <= xhigh) {
				clippedPoly.add(triX(poly.get(nextVertex), poly.get(vertex), xhigh));
				clippedPoly.add(poly.get(nextVertex));
			}
			nextVertex++;
		}
		return clippedPoly;
	}
	
	private List<Vertex3D> left(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size()) 
				nextVertex = 0;
			if(poly.get(vertex).getX() >= xlow && poly.get(nextVertex).getX() >= xlow) 
				clippedPoly.add(poly.get(nextVertex));
			if(poly.get(vertex).getX() >= xlow && poly.get(nextVertex).getX() < xlow) 
				clippedPoly.add(triX(poly.get(vertex), poly.get(nextVertex), xlow));
			if(poly.get(vertex).getX() < xlow && poly.get(nextVertex).getX() >= xlow) {
				clippedPoly.add(triX(poly.get(nextVertex), poly.get(vertex), xlow));
				clippedPoly.add(poly.get(nextVertex));
			}	
			nextVertex++;
		}
		return clippedPoly;
	}
	
	private List<Vertex3D> bot(List<Vertex3D> poly) {
		List<Vertex3D> clippedPoly = new ArrayList<Vertex3D>();
		int nextVertex = 1;
		for(int vertex = 0; vertex < poly.size(); vertex++) {
			if(nextVertex == poly.size()) 
				nextVertex = 0;	
			if(poly.get(vertex).getY() >= ylow && poly.get(nextVertex).getY() >= ylow) 
				clippedPoly.add(poly.get(nextVertex));
			if(poly.get(vertex).getY() >= ylow && poly.get(nextVertex).getY() < ylow) 
				clippedPoly.add(triY(poly.get(vertex), poly.get(nextVertex), ylow));
			if(poly.get(vertex).getY() < ylow && poly.get(nextVertex).getY() >= ylow) {
				clippedPoly.add(triY(poly.get(nextVertex), poly.get(vertex), ylow));
				clippedPoly.add(poly.get(nextVertex));
			}
			nextVertex++;
		}
		return clippedPoly;
	}
	
	private Vertex3D triX(Vertex3D in, Vertex3D out, int xplane ) {
		double dyp = ((xplane - in.getX()) / (out.getX() - in.getX())) * (out.getY() - in.getY());
		return new Vertex3D(xplane, in.getY() + dyp,out.getZ(),in.getColor());
	}
	
	private Vertex3D triY(Vertex3D in, Vertex3D out, int y ) {
		double dxp = ((y - in.getY()) / (out.getY() - in.getY())) * (out.getX() - in.getX());
		return new Vertex3D(in.getX() + dxp,y,out.getZ(),in.getColor());
	}
	
	private Vertex3D triZ(Vertex3D in, Vertex3D out, double zplane ) {
		//Calculate point on zplane between clippedPoly in and out
		//Interpolating color in 3d is hard, but we need to clip z in 3d anyway :( we just take a cheap average instead
		double dx = out.getX() - in.getX();
		double dy = out.getY() - in.getY();
		double dz = out.getZ() - in.getZ();
		Color dc = out.getColor().subtract(in.getColor());
		double dzp = zplane - in.getZ();
		double dxp = (dzp / dz) * dx;	
		double dyp = (dzp / dz) * dy;
		double distance = Math.sqrt(dx*dx + dy*dy + dz*dz);
		double clippedDistance = Math.sqrt ((in.getX() + dxp)*(in.getX() + dxp) + (in.getY() + dyp)*(in.getY() + dyp) + dzp*dzp);
		Color dcp = dc.scale(distance/clippedDistance);
		Color c1 = out.getColor().scale(.5);
		Color c2 = in.getColor().scale(.5);
		return new Vertex3D(in.getX() + dxp, in.getY() + dyp,zplane, c1.add(c2));
	}
	

}