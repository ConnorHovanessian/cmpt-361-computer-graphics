package polygon;

import geometry.Vertex3D;
import windowing.drawable.Drawable;
import windowing.graphics.Color;

public class FilledPolygonRenderer implements PolygonRenderer{
	
	@Override
	public void drawPolygon(Polygon polygon, Drawable drawable, Shader vertexShader) {
//		if(polygon.numVertices!=3) return;
//		System.out.println(polygon.numVertices);
		Vertex3D p1 = new Vertex3D(polygon.get(0).getPoint3D(),vertexShader.shade(polygon.get(0).getColor()));
		Vertex3D p2 = new Vertex3D(polygon.get(1).getPoint3D(),vertexShader.shade(polygon.get(1).getColor()));
		Vertex3D p3 = new Vertex3D(polygon.get(2).getPoint3D(),vertexShader.shade(polygon.get(2).getColor()));
		
		double x1 = p1.getX();
		double x2 = p2.getX();
		double x3 = p3.getX();

		double y1 = p1.getY();
		double y2 = p2.getY();
		double y3 = p3.getY();
		
		//Case where the triangle has a horizontal line - Check which vertex has a greater y value than another to determine top(y)
		if(y1==y2 || y2==y3 || y1 == y3)
		{
			//Arrange vertices by y value into an Array - [0] is top/bottom, [1] is left, [2] is right
			Vertex3D[] vArray = new Vertex3D[3];
			
			if (y1 > y2 && y1 > y3 || y1 < y2 && y1 < y3)
			{
				vArray[0]=p1;
				if(p2.getX()<p3.getX())
				{
				vArray[1]=p2;
				vArray[2]=p3;
				} else {
				vArray[1]=p3;
				vArray[2]=p2;
				}
			}
			if (y2 > y1 && y2 > y3 || y2 < y1 && y2 < y3)
			{
				vArray[0]=p2;
				if(p1.getX()<p3.getX())
				{
				vArray[1]=p1;
				vArray[2]=p3;
				} else {
				vArray[1]=p3;
				vArray[2]=p1;
				}
			}
			if (y3 > y1 && y3 > y2 || y3 < y2 && y3 < y1)
			{
				vArray[0]=p3;
				if(p1.getX()<p2.getX())
				{
				vArray[1]=p1;
				vArray[2]=p2;
				} else {
				vArray[1]=p2;
				vArray[2]=p1;
				}
			}
			else if(y1==y2 && y2==y3) //Case where we have three points with the same y value (but different z values), one will have a unique value
			{
				vArray[0]=p1;
				vArray[1]=p2;
				vArray[2]=p3;
			}
			
//			Time to fill in!
//			vArray[0] to vArray[1] is left, vArray[0] to vArray[2] is right
//			We are running two scanlines simultaneously so we need two sets of variables
						
			//Case where unique Y point is above
			if(vArray[0].getY() > vArray[1].getY()) 
			{
				double x0 = vArray[0].getX(); 
				double x_1 = x0;
				double x_2 = x0;
				
				double dx_1 = vArray[1].getX() - vArray[0].getX();
				double dy_1 = vArray[1].getY() - vArray[0].getY(); 
				
				double dx_2 = vArray[2].getX() - vArray[0].getX();
				double dy_2 = vArray[2].getY() - vArray[0].getY(); 
				
//				Slope is relative to x, so we put dx/dy rather than the usual dy/dx
				double m_1 = dx_1/dy_1;
				double m_2 = dx_2/dy_2;
				
//				Color blerping calcs 
//				Line from top to left
				Color c1_1 = vArray[0].getColor();
				Color c2_1 = vArray[1].getColor();
				double dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
				double dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
				double db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
				Color cdr_1 = new Color(dr_1, 0, 0);
				Color cdg_1 = new Color(0, dg_1, 0);
				Color cdb_1 = new Color(0, 0, db_1);
				Color dc_1 = new Color(dr_1, dg_1, db_1);
//				Line from top to right
				Color c1_2 = vArray[0].getColor();
				Color c2_2 = vArray[2].getColor();
				double dr_2 = (c2_2.getR()-c1_2.getR())/dy_2;
				double dg_2 = (c2_2.getG()-c1_2.getG())/dy_2;
				double db_2 = (c2_2.getB()-c1_2.getB())/dy_2;
				Color cdr_2 = new Color(dr_2, 0, 0);
				Color cdg_2 = new Color(0, dg_2, 0);
				Color cdb_2 = new Color(0, 0, db_2);
				Color dc_2 = new Color(dr_2, dg_2, db_2);
				
				Color edgeColor1 = c1_1;
				Color edgeColor2 = c1_2;

//				Z blerping
				double dz1 = (vArray[1].getZ()-vArray[0].getZ())/dy_1;
				double dz2 = (vArray[2].getZ()-vArray[0].getZ())/dy_2;
				
				double edgeZ1 = vArray[0].getZ();
				double edgeZ2 = vArray[0].getZ();
				
				for(double y = vArray[0].getY(); y > vArray[1].getY(); y--) //Iterate from top to bottom.
				{
					Color drawColor = edgeColor1;
					double dr = (edgeColor2.getR()-edgeColor1.getR())/(x_2-x_1);
					double dg = (edgeColor2.getG()-edgeColor1.getG())/(x_2-x_1);
					double db = (edgeColor2.getB()-edgeColor1.getB())/(x_2-x_1);
					Color dc = new Color(dr, dg, db);
				
					double dzMid = (edgeZ2-edgeZ1)/(x_2-x_1);
					double drawZ = edgeZ1;
					
					for(double x = x_1; x<x_2;x++) //Iterate from left to right
					{
						drawable.setPixel((int) (x+.5), (int) (y+.5), drawZ, drawColor.asARGB());
						drawColor = drawColor.add(dc);
						drawZ+=dzMid;
						
					}
					//Since we're going top to bottom, we subtract the slope instead of add it
					x_1 -= m_1; 
					x_2 -= m_2;
					edgeZ1-=dz1;
					edgeZ2-=dz2;
					//increment each edgeColor
					edgeColor1 = edgeColor1.subtract(cdr_1);
					edgeColor1 = edgeColor1.subtract(cdg_1);
					edgeColor1 = edgeColor1.subtract(cdb_1);
					edgeColor2 = edgeColor2.subtract(cdr_2);
					edgeColor2 = edgeColor2.subtract(cdg_2);
					edgeColor2 = edgeColor2.subtract(cdb_2);
					
				}
			}
			// Case where unique Y point is below
			else 
			{
				double x_1 = vArray[1].getX();
				double x_2 = vArray[2].getX();
				
				double dx_1 = vArray[1].getX() - vArray[0].getX();
				double dy_1 = vArray[1].getY() - vArray[0].getY(); 
				
				double dx_2 = vArray[2].getX() - vArray[0].getX();
				double dy_2 = vArray[2].getY() - vArray[0].getY(); 

//				Slope is relative to x, so we put dx/dy rather than the usual dy/dx
//				We know dy cannot be zero since vArray[0] has a unique y-value
				double m_1 = dx_1/dy_1;
				double m_2 = dx_2/dy_2;
				
//				Color blerping calcs
				
				Color c1_1 = vArray[1].getColor();
				Color c2_1 = vArray[0].getColor();
				double dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
				double dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
				double db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
				Color cdr_1 = new Color(dr_1, 0, 0);
				Color cdg_1 = new Color(0, dg_1, 0);
				Color cdb_1 = new Color(0, 0, db_1);

				Color c1_2 = vArray[2].getColor();
				Color c2_2 = vArray[0].getColor();
				double dr_2 = (c2_2.getR()-c1_2.getR())/dy_2;
				double dg_2 = (c2_2.getG()-c1_2.getG())/dy_2;
				double db_2 = (c2_2.getB()-c1_2.getB())/dy_2;
				Color cdr_2 = new Color(dr_2, 0, 0);
				Color cdg_2 = new Color(0, dg_2, 0);
				Color cdb_2 = new Color(0, 0, db_2);
				
				Color edgeColor1 = c1_1;
				Color edgeColor2 = c1_2;
				
				//Z blerping
				double dz1 = (vArray[1].getZ()-vArray[0].getZ())/dy_1;
				double dz2 = (vArray[2].getZ()-vArray[0].getZ())/dy_2;
				
				double edgeZ1 = vArray[1].getZ();
				double edgeZ2 = vArray[2].getZ();
				
				
				for(double y = vArray[1].getY(); y > vArray[0].getY(); y--) //Iterate from top to bottom
				{
					Color drawColor = edgeColor1;
					double dr = (edgeColor2.getR()-edgeColor1.getR())/(x_2-x_1);
					double dg = (edgeColor2.getG()-edgeColor1.getG())/(x_2-x_1);
					double db = (edgeColor2.getB()-edgeColor1.getB())/(x_2-x_1);
					
					Color cdr = new Color(dr, 0, 0);
					Color cdg = new Color(0, dg, 0);
					Color cdb = new Color(0, 0, db);
					
					double dzMid = (edgeZ2-edgeZ1)/(x_2-x_1);
					double drawZ = edgeZ1;
					
					for(double x = x_1; x<x_2;x++) //Iterate from left to right
					{
						drawable.setPixel((int) (x+.5), (int) (y+.5), (int) drawZ, drawColor.asARGB());
						drawColor = drawColor.add(cdr);
						drawColor = drawColor.add(cdg);
						drawColor = drawColor.add(cdb);
						drawZ+=dzMid;
					}
					
					x_1 -= m_1;
					x_2 -= m_2;
					edgeColor1 = edgeColor1.add(cdr_1);
					edgeColor1 = edgeColor1.add(cdg_1);
					edgeColor1 = edgeColor1.add(cdb_1);
					edgeColor2 = edgeColor2.add(cdr_2);
					edgeColor2 = edgeColor2.add(cdg_2);
					edgeColor2 = edgeColor2.add(cdb_2);
					edgeZ1 -= dz1;
					edgeZ2 -= dz2;
				}
			}
		}
	
		//Case where there are no horizontal lines in the triangle
		else
		{
			//New way
//			//Arrange triangles by y value into an Array - [0] is top, [1] is middle, [2] is bottom, set value to remember index for left and right vertices
			Vertex3D[] vArray = new Vertex3D[3];
			int leftIndex=1;
			int rightIndex=1;
			
			//If p1 is at the top
			if (y1 > y2 && y1 > y3)
			{
				vArray[0] = p1;
				if (y2 > y3) {vArray[1]=p2; vArray[2] = p3;
					if (x2 > x3) leftIndex = 2; 
					else rightIndex = 2;}
				
				else {vArray[1]=p3; vArray[2] = p2;
					if (x2 > x3) rightIndex = 2; 
					else leftIndex = 2;}
				
			}
			
			//If p2 is at the top
			if (y2 > y1 && y2 > y3)
			{
				vArray[0] = p2;
				if (y1 > y3) {vArray[1]=p1; vArray[2] = p3;
					if (x1 > x3) leftIndex = 2; 
					else rightIndex = 2;}
				
				else {vArray[1]=p3; vArray[2] = p1;
					if (x1 > x3) rightIndex = 2; 
					else leftIndex = 2;}
			}
			
			//If p3 is at the top
			if (y3 > y1 && y3 > y2)
			{
				vArray[0] = p3;
				if (y1 > y2) {vArray[1]=p1; vArray[2] = p2;
					if (x1 > x2) leftIndex = 2; 
					else rightIndex = 2;}
				
				else {vArray[1]=p2; vArray[2] = p1;
					if (x1 > x2) rightIndex = 2; 
					else leftIndex = 2;}
			}
			
			//Interpolate x of midpoint to split into two polygons with horizontal lines
			double x4;
			if(vArray[2].getX()==vArray[0].getX()) // If there is a vertical line 
				x4 = vArray[0].getX();
			else
				x4 = (vArray[0].getX()*(vArray[2].getY()-vArray[1].getY()) + vArray[2].getX()*(vArray[1].getY()-vArray[0].getY()))/(vArray[2].getY() - vArray[0].getY());
		
			//Interpolate Z and Color for new vertex
			double z4; 
			z4 = (vArray[0].getZ()*(vArray[2].getY()-vArray[1].getY()) + vArray[2].getZ()*(vArray[1].getY()-vArray[0].getY()))/(vArray[2].getY() - vArray[0].getY());
			
			Color c4;
			double c4_r = (vArray[0].getColor().getR()*(vArray[2].getY()-vArray[1].getY()) + vArray[2].getColor().getR()*(vArray[1].getY()-vArray[0].getY()))/(vArray[2].getY() - vArray[0].getY());
			double c4_g = (vArray[0].getColor().getG()*(vArray[2].getY()-vArray[1].getY()) + vArray[2].getColor().getG()*(vArray[1].getY()-vArray[0].getY()))/(vArray[2].getY() - vArray[0].getY());
			double c4_b = (vArray[0].getColor().getB()*(vArray[2].getY()-vArray[1].getY()) + vArray[2].getColor().getB()*(vArray[1].getY()-vArray[0].getY()))/(vArray[2].getY() - vArray[0].getY());
			c4 = new Color(c4_r, c4_g, c4_b);
			
			vArray[0].getColor().getR();
			//Create the two polygons and draw them
			Vertex3D p4 = new Vertex3D(x4, vArray[1].getY(), z4, c4);
			Polygon poly1 = Polygon.make(vArray[0], vArray[1], p4);
			Polygon poly2 = Polygon.make(vArray[2], vArray[1], p4);
			drawPolygon(poly1, drawable);
			drawPolygon(poly2, drawable);
			
			//Old way
			
//			//Arrange triangles by y value into an Array - [0] is top, [1] is middle, [2] is bottom, set value to remember index for left and right vertices
//			Vertex3D[] vArray = new Vertex3D[3];
//			int leftIndex=1;
//			int rightIndex=1;
//			
//			//If p1 is at the top
//			if (y1 > y2 && y1 > y3)
//			{
//				vArray[0] = p1;
//				if (y2 > y3) {vArray[1]=p2; vArray[2] = p3;
//					if (x2 > x3) leftIndex = 2; 
//					else rightIndex = 2;}
//				
//				else {vArray[1]=p3; vArray[2] = p2;
//					if (x2 > x3) rightIndex = 2; 
//					else leftIndex = 2;}
//				
//			}
//			
//			//If p2 is at the top
//			if (y2 > y1 && y2 > y3)
//			{
//				vArray[0] = p2;
//				if (y1 > y3) {vArray[1]=p1; vArray[2] = p3;
//					if (x1 > x3) leftIndex = 2; 
//					else rightIndex = 2;}
//				
//				else {vArray[1]=p3; vArray[2] = p1;
//					if (x1 > x3) rightIndex = 2; 
//					else leftIndex = 2;}
//			}
//			
//			//If p3 is at the top
//			if (y3 > y1 && y3 > y2)
//			{
//				vArray[0] = p3;
//				if (y1 > y2) {vArray[1]=p1; vArray[2] = p2;
//					if (x1 > x2) leftIndex = 2; 
//					else rightIndex = 2;}
//				
//				else {vArray[1]=p2; vArray[2] = p1;
//					if (x1 > x2) rightIndex = 2; 
//					else leftIndex = 2;}
//			}
//			
//			
////			Time to fill in!
////			We are running two scanlines simultaneously so we need two sets of variables
//			
//			//From top to left and top to right
//			//*_1 variables will be on the left, *_2 variables on the right
//			double x_1 = vArray[0].getX();
//			double x_2 = vArray[0].getX();
//			
//			double dx_1 = vArray[leftIndex].getX() - vArray[0].getX();
//			double dy_1 = vArray[leftIndex].getY() - vArray[0].getY(); 
//			
//			double dx_2 = vArray[rightIndex].getX() - vArray[0].getX();
//			double dy_2 = vArray[rightIndex].getY() - vArray[0].getY();
//
////			Slope is relative to x, so we put dx/dy rather than the usual dy/dx. 
////			We know dy cannot be 0 since if it was, this would have had a horizontal line
//			double m_1 = dx_1/dy_1;
//			double m_2 = dx_2/dy_2;
//			
//			//Color!
//			Color c1_1 = vArray[0].getColor();
//			Color c2_1 = vArray[leftIndex].getColor();
//			double dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
//			double dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
//			double db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
//			Color cdr_1 = new Color(dr_1, 0, 0);
//			Color cdg_1 = new Color(0, dg_1, 0);
//			Color cdb_1 = new Color(0, 0, db_1);
//
//			Color c1_2 = vArray[0].getColor();
//			Color c2_2 = vArray[rightIndex].getColor();
//			double dr_2 = (c2_2.getR()-c1_2.getR())/dy_2;
//			double dg_2 = (c2_2.getG()-c1_2.getG())/dy_2;
//			double db_2 = (c2_2.getB()-c1_2.getB())/dy_2;
//			Color cdr_2 = new Color(dr_2, 0, 0);
//			Color cdg_2 = new Color(0, dg_2, 0);
//			Color cdb_2 = new Color(0, 0, db_2);
//			
//			Color edgeColor1 = c1_1;
//			Color edgeColor2 = c1_2;
//			
//			if(x_1 - m_1 > x_2 - m_2)	//If edge case described in read me
//			{
//				int i=0; i++;
//				for(double y = vArray[0].getY(); y > vArray[1].getY(); y--) //Iterate from top to middle 
//				{
//					Color drawColor = edgeColor1;
//					double dr = (edgeColor1.getR()-edgeColor2.getR())/(x_2-x_1);
//					double dg = (edgeColor1.getG()-edgeColor2.getG())/(x_2-x_1);
//					double db = (edgeColor1.getB()-edgeColor2.getB())/(x_2-x_1);
//					
//					Color cdr = new Color(dr, 0, 0);
//					Color cdg = new Color(0, dg, 0);
//					Color cdb = new Color(0, 0, db);
//					 
//					for(double x = x_2; x<x_1;x++) //Iterate from left to right
//					{
//						drawable.setPixel((int) (x+.5), (int) (y+.5), (vArray[0].getZ()+vArray[1].getZ()+vArray[2].getZ() )/3, drawColor.asARGB());
//						drawColor = drawColor.add(cdr);
//						drawColor = drawColor.add(cdg);
//						drawColor = drawColor.add(cdb);
//					}
//
//					x_1 -= m_1; 
//					x_2 -= m_2;
//					edgeColor1 = edgeColor1.subtract(cdr_2);
//					edgeColor1 = edgeColor1.subtract(cdg_2);
//					edgeColor1 = edgeColor1.subtract(cdb_2);
//					edgeColor2 = edgeColor2.subtract(cdr_1);
//					edgeColor2 = edgeColor2.subtract(cdg_1);
//					edgeColor2 = edgeColor2.subtract(cdb_1);
//				}
//				
//			}
//			else
//			{
//				for(double y = vArray[0].getY(); y > vArray[1].getY(); y--) //Iterate from top to middle 
//				{
//					Color drawColor = edgeColor1;
//					double dr = (edgeColor2.getR()-edgeColor1.getR())/(x_2-x_1);
//					double dg = (edgeColor2.getG()-edgeColor1.getG())/(x_2-x_1);
//					double db = (edgeColor2.getB()-edgeColor1.getB())/(x_2-x_1);
//					
//					Color cdr = new Color(dr, 0, 0);
//					Color cdg = new Color(0, dg, 0);
//					Color cdb = new Color(0, 0, db);
//					
//					for(double x = x_1; x<x_2;x++) //Iterate from left to right
//					{
//						drawable.setPixel((int) (x+.5), (int) (y+.5), (vArray[0].getZ()+vArray[1].getZ()+vArray[2].getZ() )/3, drawColor.asARGB());
//						drawColor = drawColor.add(cdr);
//						drawColor = drawColor.add(cdg);
//						drawColor = drawColor.add(cdb);
//					}
//					x_1 -= m_1; 
//					x_2 -= m_2;
//					edgeColor1 = edgeColor1.subtract(cdr_1);
//					edgeColor1 = edgeColor1.subtract(cdg_1);
//					edgeColor1 = edgeColor1.subtract(cdb_1);
//					edgeColor2 = edgeColor2.subtract(cdr_2);
//					edgeColor2 = edgeColor2.subtract(cdg_2);
//					edgeColor2 = edgeColor2.subtract(cdb_2);
//				}
//			}
//			
//			//From middle to bottom
//			if(rightIndex == 2 ) //Case where midpoint was on the left, we need to recalculate m_1 
//			{	
//				dx_1 = vArray[2].getX() - vArray[1].getX();
//				dy_1 = vArray[2].getY() - vArray[1].getY(); 
//				m_1 = dx_1/dy_1;
//				x_1 = vArray[1].getX();
//		
//				if(x_1 - m_1 > x_2 - m_2) // If edgecase, edgecolor2 is already correct, need to fix edgecolor1 derivative
//				{
//					c1_1 = vArray[1].getColor();
//					c2_1 = vArray[2].getColor();
//					dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
//					dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
//					db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
//					cdr_1 = new Color(dr_1, 0, 0);
//					cdg_1 = new Color(0, dg_1, 0);
//					cdb_1 = new Color(0, 0, db_1);
//					edgeColor1 = c1_1;
//				}
//				else
//				{
//					c1_1 = vArray[1].getColor();
//					c2_1 = vArray[2].getColor();
//					dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
//					dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
//					db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
//					cdr_1 = new Color(dr_1, 0, 0);
//					cdg_1 = new Color(0, dg_1, 0);
//					cdb_1 = new Color(0, 0, db_1);
//					edgeColor1 = c1_1;
//				}
//			}
//			else //Case where midpoint was on the right, we need to recalculate m_2 
//			{	
//				dx_2 = vArray[2].getX() - vArray[1].getX();
//				dy_2 = vArray[2].getY() - vArray[1].getY(); 
//				m_2 = dx_2/dy_2;
//				x_2 = vArray[1].getX();
//				
//				if(x_1 - m_1 > x_2 - m_2)//edgecase 
//				{
//					c1_1 = vArray[1].getColor();
//					c2_1 = vArray[2].getColor();
//					dr_1 = (c2_1.getR()-c1_1.getR())/dy_1;
//					dg_1 = (c2_1.getG()-c1_1.getG())/dy_1;
//					db_1 = (c2_1.getB()-c1_1.getB())/dy_1;
//					cdr_1 = new Color(dr_1, 0, 0);
//					cdg_1 = new Color(0, dg_1, 0);
//					cdb_1 = new Color(0, 0, db_1);
//				}
//				else
//				{
//					c1_2 = vArray[1].getColor();
//					c2_2 = vArray[2].getColor();
//					dr_2 = (c2_2.getR()-c1_2.getR())/dy_2;
//					dg_2 = (c2_2.getG()-c1_2.getG())/dy_2;
//					db_2 = (c2_2.getB()-c1_2.getB())/dy_2;
//					cdr_2 = new Color(dr_2, 0, 0);
//					cdg_2 = new Color(0, dg_2, 0);
//					cdb_2 = new Color(0, 0, db_2);
//					edgeColor2 = c1_2;
//				}
//				
//			}
//			
//			//Now that we've adjusted our slope, we can continue filling in from the middle to the bottom of the polygon
//			if(x_1 - m_1 > x_2 - m_2)	//If edge case described in read me
//			{
//				for(double y = vArray[1].getY(); y > vArray[2].getY(); y--) //Iterate from middle to bottom
//				{
//					Color drawColor = edgeColor2;
//					double dr = (edgeColor1.getR()-edgeColor2.getR())/(x_1-x_2);
//					double dg = (edgeColor1.getG()-edgeColor2.getG())/(x_1-x_2);
//					double db = (edgeColor1.getB()-edgeColor2.getB())/(x_1-x_2);
//					
//					Color cdr = new Color(dr, 0, 0);
//					Color cdg = new Color(0, dg, 0);
//					Color cdb = new Color(0, 0, db);
//					
//					for(double x = x_2; x<x_1;x++) //Iterate from right to left
//					{
//						drawable.setPixel((int) (x+.5), (int) (y+.5), (vArray[0].getZ()+vArray[1].getZ()+vArray[2].getZ() )/3, drawColor.asARGB());
//						drawColor = drawColor.add(cdr);
//						drawColor = drawColor.add(cdg);
//						drawColor = drawColor.add(cdb);
//					}
//					
//					x_1 -= m_1;
//					x_2 -= m_2;
//					edgeColor1 = edgeColor1.subtract(cdr_1);
//					edgeColor1 = edgeColor1.subtract(cdg_1);
//					edgeColor1 = edgeColor1.subtract(cdb_1);
//					edgeColor2 = edgeColor2.subtract(cdr_2);
//					edgeColor2 = edgeColor2.subtract(cdg_2);
//					edgeColor2 = edgeColor2.subtract(cdb_2);
//				}
//			}
//			else
//			{
//				for(double y = vArray[1].getY(); y > vArray[2].getY(); y--) //Iterate from middle to bottom 
//				{
//					Color drawColor = edgeColor1;
//					double dr = (edgeColor2.getR()-edgeColor1.getR())/(x_2-x_1);
//					double dg = (edgeColor2.getG()-edgeColor1.getG())/(x_2-x_1);
//					double db = (edgeColor2.getB()-edgeColor1.getB())/(x_2-x_1);
//					
//					Color cdr = new Color(dr, 0, 0);
//					Color cdg = new Color(0, dg, 0);
//					Color cdb = new Color(0, 0, db);
//					
//					for(double x = x_1; x<x_2;x++) //Iterate from left to right
//					{
//						drawable.setPixel((int) (x+.5), (int) (y+.5), (vArray[0].getZ()+vArray[1].getZ()+vArray[2].getZ() )/3, drawColor.asARGB());
//						drawColor = drawColor.add(cdr);
//						drawColor = drawColor.add(cdg);
//						drawColor = drawColor.add(cdb);
//					}
//					
//	
//					x_1 -= m_1; 
//					x_2 -= m_2;
//					edgeColor1 = edgeColor1.subtract(cdr_1);
//					edgeColor1 = edgeColor1.subtract(cdg_1);
//					edgeColor1 = edgeColor1.subtract(cdb_1);
//					edgeColor2 = edgeColor2.subtract(cdr_2);
//					edgeColor2 = edgeColor2.subtract(cdg_2);
//					edgeColor2 = edgeColor2.subtract(cdb_2);
//				}
//			}
//			
		}
		
	}

	public static PolygonRenderer make() {
		return new FilledPolygonRenderer();
	}
	
}
