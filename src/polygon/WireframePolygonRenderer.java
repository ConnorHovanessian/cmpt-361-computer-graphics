package polygon;

import com.sun.prism.paint.Color;
import client.Client;
import geometry.Vertex3D;
import line.DDALineRenderer;
import line.LineRenderer;
import windowing.drawable.Drawable;

public class WireframePolygonRenderer implements PolygonRenderer {
	private PolygonRenderer FilledPolygonRenderer;
	
	@Override
	public void drawPolygon(Polygon polygon, Drawable drawable, Shader vertexShader) {
		Vertex3D p1 = polygon.get(0);
		Vertex3D p2 = polygon.get(1);
		Vertex3D p3 = polygon.get(2);
		int color = p1.getColor().asARGB();
		
		LineRenderer DDA = DDALineRenderer.make();
		
//			To draw wireframe
			DDA.drawLine(p1, p2, drawable); 
			DDA.drawLine(p1, p3, drawable); 
			DDA.drawLine(p2, p3, drawable); 
	}
	
	public static PolygonRenderer make() {
		return new WireframePolygonRenderer();
	}

}
