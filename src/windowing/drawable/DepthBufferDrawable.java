package windowing.drawable;

import geometry.Point2D;
import windowing.graphics.Color;
import windowing.graphics.Dimensions;

public class DepthBufferDrawable  extends DrawableDecorator{

	public static final int ARGB_WHITE = new Color(1.0, 1.0, 1.0).asARGB();
	private Drawable image;
	private Integer width;
	private Integer height;
	private Integer backClippingPlane;
	private Integer frontClippingPlane;
	private Color baseColor = new Color(1.0,1.0,1.0);
	private double[][] zBuffer;
	  
  	public DepthBufferDrawable(Drawable delegate)
  	{
	  super(delegate);
	  return;
  	}
  
	public DepthBufferDrawable(Drawable fullPanel, int hither, int yon, Color color) {
		super(fullPanel);
		this.frontClippingPlane = hither;
		this.backClippingPlane = yon;
		baseColor = color;
		fullPanel = new InvertedYDrawable(fullPanel);
		fullPanel = new TranslatingDrawable(fullPanel, new Point2D(0.0, 0.0), new Dimensions(650, 650));
		fullPanel = new PixelClippingDrawable(fullPanel);
		fullPanel = new ZBufferDrawable(fullPanel);
		fullPanel = new ColoredDrawable(fullPanel, color.asARGB());
		
		image = fullPanel;
		
		return;
	}
	

	@Override
	public void setPixel(int x, int y, double z, int argbColor) {
		Color color = Color.fromARGB(argbColor);
		if(backClippingPlane!=null)
		{
			double d = 1-Math.abs(z/(backClippingPlane-(0-frontClippingPlane)));
			color=color.scale(d);
		}
		delegate.setPixel(x, y, z, color.asARGB());
	}
	
	@Override
	public void setPixelWithCoverage(int x, int y, double z, int argbColor, double coverage) {
		Color color = Color.fromARGB(argbColor);
		double d = 1-Math.abs(z/(backClippingPlane-frontClippingPlane));
		color=color.scale(d);
		delegate.setPixelWithCoverage(x, y, z, color.asARGB(), coverage);
	}		 
	
}
