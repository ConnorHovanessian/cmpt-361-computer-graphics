package windowing.drawable;

public class ColoredDrawable
  extends DrawableDecorator
{
  private final Integer color;
  
  public ColoredDrawable(Drawable delegate, Integer color)
  {
    super(delegate);
    this.color = color;
  }
  
  public void clear()
  {
    fill(this.color.intValue(), Double.MAX_VALUE);
  }
}
