package windowing.drawable;

public class ZBufferDrawable  extends DrawableDecorator{

	private Integer width;
	private Integer height;
	private final int backClippingPlane = -200;
	private double[][] zBuffer;
	  
  	public ZBufferDrawable(Drawable delegate)
  	{
	  super(delegate);
	  this.width = delegate.getWidth();
	  this.height = delegate.getHeight();
	  zBuffer = new double[width][height];
	  
	  for(int i=0;i<width;i++)
		  for(int j=0;j<height;j++)
			  zBuffer[i][j] = backClippingPlane;
	  return;
  	}
  
	@Override
	public void setPixel(int x, int y, double z, int argbColor) {
		if(x>width-50||y>height-50||x<50||y<50)
			return;
		if((10000>z&&z>0))
		{
			zBuffer[x][y] = z;
		}
		if(z>zBuffer[x][y])
		{
			delegate.setPixel(x, y, z, argbColor);
			if(z==Double.MAX_VALUE) zBuffer[x][y]= backClippingPlane; //If we're using clear function, we don't want to set the clipping plane to MaxDouble, we want it to be at the backClippingPlane
			else zBuffer[x][y]= z;
		}
		else 
			return;	
	}
	
	@Override
	public void setPixelWithCoverage(int x, int y, double z, int argbColor, double coverage) {
		if(z>zBuffer[x][y])
		{
			delegate.setPixelWithCoverage(x, y, z, argbColor, coverage);
			if(z==Double.MAX_VALUE) zBuffer[x][y]= backClippingPlane; //If we're using clear function, we don't want to set the clipping plane to MaxDouble, we want it to be at the backClippingPlane
			else zBuffer[x][y]= z;
		}
		else return;
	}		 
	
	@Override
	public void clear()
	{
		for(int i=0;i<width;i++)
			for(int j=0;j<height;j++)
				zBuffer[i][j] = backClippingPlane;
	}
	
}
