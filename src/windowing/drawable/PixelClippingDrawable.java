package windowing.drawable;

import geometry.Point2D;
import windowing.graphics.Color;
import windowing.graphics.Dimensions;

public class PixelClippingDrawable  extends DrawableDecorator{

	private Drawable image;
	private Integer width;
	private Integer height;
	  
  	public PixelClippingDrawable(Drawable delegate)
  	{
	  super(delegate);
	  width = delegate.getWidth();
	  height = delegate.getHeight();
	  return;
  	}

	@Override
	public void setPixel(int x, int y, double z, int argbColor) {
		if(x>width||y>height||x<50||y<50) //Check for x,y <50 since there are 50 pixel margins 
			return;
		else 
		{
			delegate.setPixel(x, y, z, argbColor);
		}
	
	}
	
	@Override
	public void setPixelWithCoverage(int x, int y, double z, int argbColor, double coverage) {
		if(x>width||y>height||x<0||y<0)
			return;
		else 
		{
			delegate.setPixel(x, y, z, argbColor);
		}
	}		 
	
}
