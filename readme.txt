By Connor Hovanessian
301256742

Hi,

The most noticeable leftover is that it looks like the clipper is a little misaligned. This is most visible in pages 2 & 3, but otherwise it seems to be doing its job.

There are also some noticable artifacts on the dummy. 
It looks like I'm overdrawing lines which make the wireframe a little harder to see, and there's a strange artifact on the filled in version on the arm.
The UFO on my custom page seems to be artifact free in both versions, so I'm not 100% sure as to where the problems could be coming from. 

This was developed on Windows 10 with Eclipse. 

To pass arguments I used Eclipse's GUI for the most part, but I also created a .JAR file to do things the old fasioned way for testing.
The JAR file also worked, so I think that part should be good. You don't need to include the extension, so an example command would be: java -jar a3.jar pageD

-----------------------

Originally I submitted this on Monday, but I actually had a dream about some things I could have done differently so I made a few changes. 
I realized the clipper wasn't great when there are multiple outside-outside vertex edges in a row, this is apparent on page B and C, for the big green polygon. 
I tried to implement a way to remember the last vertex drawn, then draw from that vertex after a series of outside-outside edges, but it didn't pan out.
It did make page C have the correct green polygon, but it suffered elsewhere so I omitted those changes. Otherwise I resolved some other problems with the clipper, and optimized simpInterpreter a little bit as well. 


Thanks,
Connor
